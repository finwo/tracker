FROM node:alpine
RUN npm install -g bittorrent-tracker
EXPOSE 8080
CMD bittorrent-tracker --http --ws --port 8080
